package bionexo

import bionexo.RoutesLoader.RoutesInformation
import bionexo.RoutesOperation._
import bionexo.exception.NoSuchRouteException

/**
  * Created by arssouza on 03/12/16.
  */
object Challange {

  def calculateOutput1(routesInformation: RoutesInformation): String =

    try {
      calculateCost(routesInformation, List('A', 'D', 'E')).toString
    } catch {
      case NoSuchRouteException => "NO SUCH ROUTE"
    }

  def calculateOutput2(routesInformation: RoutesInformation): String =
    try {
      calculateCost(routesInformation, List('A', 'F', 'E')).toString
    } catch {
      case NoSuchRouteException => "NO SUCH ROUTE"
    }

  def calculateOutput3(routesInformation: RoutesInformation): String =
    try {
      calculateCost(routesInformation, List('E', 'C', 'B')).toString
    } catch {
      case NoSuchRouteException => "NO SUCH ROUTE"
    }

  def calculateOutput4(routesInformation: RoutesInformation): String =
    try {
      calculateCost(routesInformation, List('B', 'D', 'F', 'E')).toString
    } catch {
      case NoSuchRouteException => "NO SUCH ROUTE"
    }

  def calculateOutput5(routesInformation: RoutesInformation): String =
    try {
      calculateCost(routesInformation, List('F', 'C')).toString
    } catch {
      case NoSuchRouteException => "NO SUCH ROUTE"
    }

  def calculateOutput6(routesInformation: RoutesInformation): String = {
    val counter = countArriving(routesInformation, 'C')
    if (counter > 0)
      counter.toString
    else
      "NO SUCH ROUTE"
  }

  def calculateOutput7(routesInformation: RoutesInformation): Int =
    countRoutesStartWith(routesInformation, 'B', 'A', 5)

  def calculateOutput8(routesInformation: RoutesInformation): Int =
    countRoutesStartWith(routesInformation, 'A', 'A', 3, 3)

  def calculateOutput9(routesInformation: RoutesInformation): Int =
    shortestPathCost(routesInformation, 'A', 'E')

  def calculateOutput10(routesInformation: RoutesInformation): Int =
    shortestPathCost(routesInformation, 'C', 'E')

  def calculateOutput11(routesInformation: RoutesInformation): Int =
    countRoutesWithMaxCostStartingWith(routesInformation, 'A', 'B', 40)

  def calculateOutput12(routesInformation: RoutesInformation): Int =
    countRoutesWithMaxCostStartingWith(routesInformation, 'E', 'D', 60)

}
