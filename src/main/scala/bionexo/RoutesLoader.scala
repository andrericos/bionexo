package bionexo

import scala.annotation.tailrec

/**
  * Created by arssouza on 02/12/16.
  */
object RoutesLoader {

  type RoutesInformation = Map[Char, RoutesFromSingleOrigin]

  def loadRoutes(routes: String): RoutesInformation = {
    @tailrec
    def accRoutes(rawRoutes: List[String], routes: Map[Char, RoutesFromSingleOrigin]): Map[Char, RoutesFromSingleOrigin] = {
      val Pattern = "([A-F])([A-F])(\\d*)".r
      rawRoutes match {
        case Pattern(origin, destination, costString) :: remaining =>
          val cost = costString.toInt
          val newRoute = routes get origin(0) match {
            case Some(currentRoute) => currentRoute.addTarget(destination(0), cost)
            case None => new RoutesFromSingleOrigin(origin(0), Map(destination(0) -> cost))
          }

          accRoutes(remaining, routes + (origin(0) -> newRoute))

        case _ => routes
      }
    }

    accRoutes(routes.split(",").toList map (s => s.trim), Map())
  }

}
