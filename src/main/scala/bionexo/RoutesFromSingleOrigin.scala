package bionexo

/**
  * Created by arssouza on 02/12/16.
  */
class RoutesFromSingleOrigin(origin: Char, destinationsCost: Map[Char, Int]) {

  def getCost(destination: Char): Option[Int] = destinationsCost get destination

  def reaches_?(spot: Char): Boolean = destinationsCost contains spot

  def availableDestinations(): List[(Char, Int)] = destinationsCost.toList

  def addTarget(destination: Char, cost: Int) =
    new RoutesFromSingleOrigin(origin, destinationsCost + (destination -> cost))

}
