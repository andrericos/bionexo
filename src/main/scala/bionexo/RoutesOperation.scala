package bionexo

import bionexo.RoutesLoader.RoutesInformation
import bionexo.exception.NoSuchRouteException

import scala.annotation.tailrec

/**
  * Created by arssouza on 02/12/16.
  */
object RoutesOperation {

  def calculateCost(routesInformation: RoutesInformation, routeSteps: List[Char]): Int = {
    @tailrec
    def acc(routeSteps: List[Char], accumulator: Int): Int =
      routeSteps match {
        case origin :: target :: remainingRoute => routesInformation get origin match {
          case Some(originInformation) => originInformation getCost target match {
            case None => throw NoSuchRouteException
            case Some(cost) => acc(target :: remainingRoute, accumulator + cost)
          }
          case _ => throw NoSuchRouteException
        }
        case _ => accumulator
      }

    acc(routeSteps, 0)
  }

  def countArriving(routesInformation: RoutesInformation, spot: Char): Int =
    routesInformation.values count (originInformation => originInformation reaches_? spot)

  def countRoutesStartWith(routesInformation: RoutesInformation, origin: Char, destination: Char, minStops: Int, maxStops: Int): Int =
    countRoutesStartWith(routesInformation, origin, destination, minStops, maxStops, Int.MaxValue)

  def countRoutesStartWith(routesInformation: RoutesInformation, origin: Char, destination: Char, maxStops: Int): Int =
    countRoutesStartWith(routesInformation, origin, destination, 0, maxStops, Int.MaxValue)

  def countRoutesWithMaxCostStartingWith(routesInformation: RoutesInformation, origin: Char, destination: Char, maxCost: Int): Int =
    countRoutesStartWith(routesInformation, origin, destination, 0, Int.MaxValue, maxCost)

  def countRoutesStartWith(routesInformation: RoutesInformation, origin: Char, destination: Char, minStops: Int, maxStops: Int, maxCost: Int): Int = {
    def recur(currentSpot: Char, currentRouteCost: Int, currentStops: Int): Int =
      if (currentStops > maxStops || currentRouteCost >= maxCost) {
        0
      } else {
        val childrenValue = routesInformation get currentSpot match {
          case None => 0
          case Some(originInformation) =>
            (for (
              (nextAvailableDestination, cost) <- originInformation.availableDestinations()
            ) yield recur(nextAvailableDestination, currentRouteCost + cost, currentStops + 1)).sum
        }

        if (currentSpot == destination && currentStops >= minStops)
          childrenValue + 1
        else
          childrenValue
      }

    recur(origin, 0, 0)
  }

  def shortestPathCost(routesInformation: RoutesInformation, origin: Char, destination: Char): Int = {
    def recur(currentCost: Int, currentSpot: Char, alreadyVisited: Set[Char]): Int =
      if (currentSpot == destination)
        currentCost
      else if (alreadyVisited contains currentSpot)
        Int.MaxValue
      else
        routesInformation get currentSpot match {
          case None => Int.MaxValue
          case Some(originInformation) =>
            (for (
              (nextAvailableDestination, cost) <- originInformation.availableDestinations()
            ) yield recur(currentCost + cost, nextAvailableDestination, alreadyVisited + currentSpot)).min
        }

    recur(0, origin, Set())
  }
}
