package bionexo.test

import bionexo.Challange._
import bionexo.RoutesFromSingleOrigin
import bionexo.RoutesLoader._
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by arssouza on 03/12/16.
  */
@RunWith(classOf[JUnitRunner])
class DefaultRouteOperationTest extends FunSuite {

  trait BaseTest {
    val defaultRouteInformation: Map[Char, RoutesFromSingleOrigin] = loadRoutes("AD4, DE1, EC8, CB2, BA6, AC9, DF7, FC5, FE9, BD3, FA3")
  }


  test("Output #1 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput1(defaultRouteInformation) === "5")
    }
  }

  test("Output #2 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput2(defaultRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #3 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput3(defaultRouteInformation) === "10")
    }
  }

  test("Output #4 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput4(defaultRouteInformation) === "19")
    }
  }

  test("Output #5 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput5(defaultRouteInformation) === "5")
    }
  }

  test("Output #6 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput6(defaultRouteInformation) === "3")
    }
  }

  test("Output #7 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput7(defaultRouteInformation) === 6)
    }
  }

  test("Output #8 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput8(defaultRouteInformation) === 2)
    }
  }

  test("Output #9 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput9(defaultRouteInformation) === 5)
    }
  }

  test("Output #10 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput10(defaultRouteInformation) === 6)
    }
  }

  test("Output #11 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput11(defaultRouteInformation) === 27)
    }
  }

  test("Output #12 Test: Default Test") {
    new BaseTest {
      assert(calculateOutput12(defaultRouteInformation) === 137)
    }
  }
}
