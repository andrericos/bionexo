package bionexo.test

import bionexo.Challange._
import bionexo.RoutesFromSingleOrigin
import bionexo.RoutesLoader._
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by arssouza on 04/12/16.
  */
@RunWith(classOf[JUnitRunner])
class EmptyRoutingInformationOperationTest extends FunSuite {

  trait BaseTest {
    val emptyRouteInformation: Map[Char, RoutesFromSingleOrigin] = loadRoutes("")
  }


  test("Output #1 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput1(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #2 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput2(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #3 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput3(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #4 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput4(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #5 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput5(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #6 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput6(emptyRouteInformation) === "NO SUCH ROUTE")
    }
  }

  test("Output #7 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput7(emptyRouteInformation) === 0)
    }
  }

  test("Output #8 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput8(emptyRouteInformation) === 0)
    }
  }

  test("Output #9 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput9(emptyRouteInformation) === Int.MaxValue)
    }
  }

  test("Output #10 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput10(emptyRouteInformation) === Int.MaxValue)
    }
  }

  test("Output #11 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput11(emptyRouteInformation) === 0)
    }
  }

  test("Output #12 Test: Empty Route Information Test") {
    new BaseTest {
      assert(calculateOutput12(emptyRouteInformation) === 0)
    }
  }
}
